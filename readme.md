
### Dans ce sens, voici l'énoncé de l'exercice (durée estimée 2 à 3 heures - à me retourner maximum mercredi 18/05/22) :

# Objectif :
### Créer un mini site de gestion de listes de tâches en PHP/MySQL, en utilisant le framework Symfony 5

# Entités :
Une liste est composée d'un nom (requis) et d'un ensemble de tâches.
Une tâche est composée d'un titre (requis) et d'un état (fait ou à faire).

# Listing des listes :
A l'arrivée sur le site, l'utilisateur peut consulter les listes existantes. Le listing affiche pour chaque liste son nom, ainsi que le nombre de tâches qu'elle contient.
Chaque liste affiche également un bouton permettant d'accéder au détail de celle-ci et un autre pour supprimer la liste.
Par ailleurs, un formulaire permet de créer une nouvelle liste en renseignant un nom. Deux listes ne peuvent pas avoir le même nom. Après validation du formulaire de création de liste, l'utilisateur est redirigé vers la vue de détail de cette nouvelle liste.

# Détail d'une liste :
Dans la vue de détail d'une liste, l'utilisateur peut éditer son nom et voir le listing des tâches de cette liste. Le listing affiche pour chaque tâche son titre. Chaque tâche présente également un bouton permettant de l'éditer et un autre bouton pour changer rapidement son état (fait ou à faire). Les tâches à faire doivent être visuellement différenciables des tâches faites. Sous la liste des tâches, un bouton permet de supprimer définitivement toutes les tâches dans l'état "fait". Enfin, un bouton permet également d'accéder au formulaire de création d'une nouvelle tâche.

# Création/Édition de tache :
Le formulaire de création/édition de tâche se compose d'un champ "titre" et d'une case à cocher indiquant l'état de la tâche (fait ou à faire). Après validation du formulaire, l'utilisateur est redirigé vers la vue de détail de la liste dans laquelle cette tâche a été créée.

---

    Une attention particulière sera portée à la propreté du code et à l'utilisation
    appropriée du framework. Bien que l'esthétique du mini site ne soit pas jugée, 
    son ergonomie doit être soignée (l'utilisation d'un framework CSS tel 
    que Bootstrap est parfaitement envisageable).

### Le code source et le dump de la base de données doivent m'être renvoyés par e-mail, avec indication du temps passé.


## UPDATE

Voici nos demandes :

~~Mettre en place des messages de succès après une action correctement effectuée.~~

~~Une erreur 500 existe dans le code et correspond à un problème de validation des champs de formulaires côté serveur lorsque l'on retire la validation HTML côté client. Il faudrait corriger cela.~~

~~Mettre en place toutes les actions d'écriture en POST plutôt qu'en GET.~~

~~Mettre en place la protection CSRF sur les actions d'écriture.~~

~~Optimiser le process de suppression des tâches terminées et passer par une fonction spécifique dans le Repository pour la récupération des éléments à supprimer.~~

~~Corriger les problèmes de "franglais" au niveau de l'interface (tout en français ou anglais) et au niveau du code (tout en anglais). Choix libre pour les URLs du moment que c'est uniforme.~~

Merci d'avance,