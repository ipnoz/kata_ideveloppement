<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\TaskRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity(repositoryClass=TaskRepository::class)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class Task
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private string $title;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $state = false;

    /**
     * @ORM\ManyToOne(targetEntity=TaskList::class, inversedBy="tasks")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private TaskList $list;


    public function getId(): ?int
    {
        return $this->id ?? null;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function isDone(): bool
    {
        return $this->state;
    }

    public function isState(): bool
    {
        return $this->state;
    }

    public function setState(bool $state): void
    {
        $this->state = $state;
    }

    public function getList(): TaskList
    {
        return $this->list;
    }

    public function setList(TaskList $list): void
    {
        $this->list = $list;
    }
}
