<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table()
 * @ORM\Entity()
 * @UniqueEntity(fields={"title"}, message="This taskList title is already in use")
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TaskList
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(unique=true)
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     */
    private string $title;

    /**
     * @var PersistentCollection
     *
     * @ORM\OneToMany(targetEntity=Task::class, mappedBy="list")
     */
    private $tasks;


    public function getId(): ?int
    {
        return $this->id ?? null;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTasks()
    {
        return $this->tasks;
    }

    public function setTasks($tasks): void
    {
        $this->tasks = $tasks;
    }

    public function countTasks(): int
    {
        return $this->getTasks()->count();
    }
}
