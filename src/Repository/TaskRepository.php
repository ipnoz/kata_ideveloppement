<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function deleteAllPerformed(int $taskListId = null): void
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->delete()
            ->where('t.state = :state')
            ->setParameter('state', true)
        ;

        if (\is_int($taskListId)) {
            $queryBuilder
                ->andWhere('t.list = :listId')
                ->setParameter('listId', $taskListId)
            ;
        }

        $queryBuilder->getQuery()->execute();
    }
}
