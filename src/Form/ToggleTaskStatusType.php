<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class ToggleTaskStatusType extends AbstractType
{
    const BLOCK_PREFIX = 'toggle_task_status';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('submit', SubmitType::class, [
                'label' => 'Change state',
                'attr' => [
                    'class' => 'btn-secondary',
                ],
            ])
        ;
    }

    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
