<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TaskList;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TaskListType extends AbstractType
{
    const BLOCK_PREFIX = 'task_list';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        /** @var TaskList $taskList */
        $taskList = $builder->getData();

        $builder
            ->add('title')
            ->add('submit', SubmitType::class, [
                'label' => $taskList->getId() ? 'Edit the title' : 'Create a list',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TaskList::class,
        ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return self::BLOCK_PREFIX;
    }
}
