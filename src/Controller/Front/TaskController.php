<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\Task;
use App\Entity\TaskList;
use App\Form\DeleteAllTasksPerformedType;
use App\Form\ToggleTaskStatusType;
use App\Form\TaskType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class TaskController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/task/create/{id}", name="front_task_create", methods={"GET", "POST"})
     */
    public function createTaskAction(TaskList $taskList, Request $request): Response
    {
        $task = new Task();
        $task->setList($taskList);

        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($task);
            $this->entityManager->flush();
            $this->addFlash('success', 'Task created');
            return $this->redirectToRoute('front_taskList_show', ['id' => $taskList->getId()]);
        }

        return $this->render('Page/task.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/task/edit/{id}", name="front_task_edit", methods={"GET", "POST"})
     */
    public function editTaskAction(Task $task, Request $request): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            $this->addFlash('success', 'Task edited');
            return $this->redirectToRoute('front_taskList_show', ['id' => $task->getList()->getId()]);
        }

        return $this->render('Page/task.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/task/state/{id}", name="front_task_state", methods={"POST"})
     */
    public function stateAction(Task $task, Request $request): Response
    {
        $form = $this->createForm(ToggleTaskStatusType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $task->setState(! $task->isState());
            $this->entityManager->flush();
            $this->addFlash('success', 'Task state is '.($task->isDone() ? 'done' : 'to do'));
        }

        return $this->redirectToRoute('front_taskList_show', ['id' => $task->getList()->getId()]);
    }

    /**
     * @Route("/tasks/deleteAllPerformed/{id}", name="front_task_delete_all_performed", methods={"POST"})
     */
    public function deleteAllTasksPerformedAction(TaskList $taskList, Request $request): Response
    {
        $form = $this->createForm(DeleteAllTasksPerformedType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->getRepository(Task::class)->deleteAllPerformed($taskList->getId());
            $this->addFlash('success', 'All tasks performed have been deleted');
        }

        return $this->redirectToRoute('front_taskList_show', ['id' => $taskList->getId()]);
    }
}
