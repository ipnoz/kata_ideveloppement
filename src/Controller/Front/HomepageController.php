<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\TaskList;
use App\Form\DeleteTaskListType;
use App\Form\TaskListType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class HomepageController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("", name="front_homepage", methods={"GET", "POST"})
     */
    public function homepageAction(Request $request): Response
    {
        $taskList = new TaskList();
        $form = $this->createForm(TaskListType::class, $taskList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($taskList);
            $this->entityManager->flush();
            $this->addFlash('success', 'TaskList created');
            return $this->redirectToRoute('front_taskList_show', ['id' => $taskList->getId()]);
        }

        $taskLists = $this->entityManager->getRepository(TaskList::class)->findAll();

        return $this->render('Page/homepage.html.twig', [
            'form' => $form->createView(),
            'taskLists' => $taskLists,
            'deleteForms' => $this->createDeleteForms($taskLists),
        ]);
    }

    private function createDeleteForms(array $taskLists): array
    {
        $forms = [];

        foreach ($taskLists as $taskList) {

            $path = $this->generateUrl('front_taskList_delete', ['id' => $taskList->getId()]);
            $deleteForm = $this->createForm(DeleteTaskListType::class, null, ['action' => $path]);

            $forms[$taskList->getId()] = $deleteForm->createView();
        }

        return $forms;
    }
}
