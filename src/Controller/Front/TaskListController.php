<?php

declare(strict_types=1);

namespace App\Controller\Front;

use App\Entity\TaskList;
use App\Form\DeleteAllTasksPerformedType;
use App\Form\DeleteTaskListType;
use App\Form\TaskListType;
use App\Form\ToggleTaskStatusType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
final class TaskListController extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/taskList/show/{id}", name="front_taskList_show", methods={"GET", "POST"})
     */
    public function showTaskListAction(TaskList $taskList, Request $request): Response
    {
        $form = $this->createForm(TaskListType::class, $taskList);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->flush();
            $this->addFlash('success', 'TaskList edited');
            return $this->redirectToRoute('front_taskList_show', ['id' => $taskList->getId()]);
        }

        $path = $this->generateUrl('front_task_delete_all_performed', ['id' => $taskList->getId()]);
        $deleteAllTasksPerformedForm = $this->createForm(DeleteAllTasksPerformedType::class, null, ['action' => $path]);

        return $this->render('Page/taskList.html.twig', [
            'taskList' => $taskList,
            'form' => $form->createView(),
            'toggleTaskForms' => $this->createToggleTaskStateForms($taskList),
            'deleteAllTasksPerformedForm' => $deleteAllTasksPerformedForm->createView(),
        ]);
    }

    /**
     * @Route("/taskList/delete/{id}", name="front_taskList_delete", methods={"POST"})
     */
    public function deleteTaskListAction(TaskList $taskList, Request $request): Response
    {
        $form = $this->createForm(DeleteTaskListType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->remove($taskList);
            $this->entityManager->flush();
            $this->addFlash('success', 'TaskList has been deleted');
        }

        return $this->redirectToRoute('front_homepage');
    }

    private function createToggleTaskStateForms(TaskList $taskList): array
    {
        $forms = [];

        foreach ($taskList->getTasks() as $task) {

            $path = $this->generateUrl('front_task_state', ['id' => $task->getId()]);
            $form = $this->createForm(ToggleTaskStatusType::class, null, ['action' => $path]);

            $forms[$task->getId()] = $form->createView();
        }

        return $forms;
    }
}
