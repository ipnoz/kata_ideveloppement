<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\Task;
use App\Entity\TaskList;
use App\Form\TaskListType;
use App\Form\TaskType;
use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TaskListCest
{
    public function it_require_a_valid_task_list(FunctionalTester $I): void
    {
        // Given I have no taskList in repository

        // When I'm trying to get the evil number
        $I->amOnPage('/taskList/show/666');

        // Then I get 404 error page
        $I->seeResponseCodeIs(404);
    }

    public function it_show_a_task_list(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and 3 Task
        $I->haveInRepository($taskList = new TaskList(), ['title' => 'Test']);
        $I->haveInRepository(Task::class, ['title' => 'Task 1', 'list' => $taskList, 'state' => false]);
        $I->haveInRepository(Task::class, ['title' => 'Task 2', 'list' => $taskList, 'state' => true]);
        $I->haveInRepository(Task::class, ['title' => 'Task 3', 'list' => $taskList, 'state' => false]);

        // When I'm trying to get task list
        $I->amOnPage('/taskList/show/'.$taskList->getId());

        // Then I see the task list page
        $I->seeResponseCodeIs(200);
        $I->seeInTitle('TaskList Test - ideveloppement');
        $I->see('TaskList : Test', 'table caption');
        $I->see('Task 1', 'table th.alert-danger');
        $I->see('Task 2', 'table th.alert-secondary');
        $I->see('Task 3', 'table th.alert-danger');
    }

    public function it_edit_task_list_title(FunctionalTester $I): void
    {
        // Given I have a taskList in repository
        $id = $I->haveInRepository(TaskList::class, ['title' => 'Test']);
        $I->amOnPage('/taskList/show/'.$id);
        $I->see('TaskList : Test', 'table caption');

        // When I'm trying to get task list
        $I->submitSymfonyForm(TaskListType::BLOCK_PREFIX, ['[title]' => 'new title']);

        // Then I see the task list page
        $I->seeInTitle('new title - ideveloppement');
        $I->see('TaskList : new title', 'table caption');
    }

    public function it_click_on_a_task_in_the_taks_list(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and a task
        $I->haveInRepository($taskList = new TaskList(), ['title' => 'Test']);
        $I->haveInRepository(Task::class, ['title' => 'Task 1', 'list' => $taskList]);
        $I->amOnPage('/taskList/show/'.$taskList->getId());

        // When I click on the edit button
        $I->click('Edit', 'table td a');

        // Then I see I'm on the edit task page
        $I->seeInField('form input[name="'.TaskType::BLOCK_PREFIX.'[title]"]', 'Task 1');
    }

    public function it_click_on_a_change_task_state_in_the_task_list_table(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and a task
        $I->haveInRepository($taskList = new TaskList(), ['title' => 'Test taskList']);
        $I->haveInRepository(new Task(), ['title' => 'Task test', 'list' => $taskList]);
        $I->amOnPage('/taskList/show/'.$taskList->getId());
        $I->see('Task test', 'table th.alert-danger');

        // When I click on the button to change the task state
        $I->click('Change state', 'table tr [type=submit]');

        // Then I see the task in the Tasks list page
        $I->seeCurrentRouteIs('front_taskList_show', ['id' => $taskList->getId()]);
        $I->see('Task test', 'table th.alert-secondary');
    }

    public function it_click_on_remove_all_tasks_performed_button(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and 4 tasks
        $I->haveInRepository($taskList = new TaskList(), ['title' => 'Test taskList']);
        $I->haveInRepository(new Task(), ['title' => 'Task 1', 'list' => $taskList, 'state' => false]);
        $I->haveInRepository(new Task(), ['title' => 'Task 2', 'list' => $taskList, 'state' => true]);
        $I->haveInRepository(new Task(), ['title' => 'Task 3', 'list' => $taskList, 'state' => false]);
        $I->haveInRepository(new Task(), ['title' => 'Task 4', 'list' => $taskList, 'state' => true]);
        $I->amOnPage('/taskList/show/'.$taskList->getId());

        // When I click on the button to remove all tasks performed
        $I->click('Remove all tasks performed', 'form [type=submit]');

        // Then I see performed tasks has been removed
        $I->seeCurrentRouteIs('front_taskList_show', ['id' => $taskList->getId()]);
        $I->see('Task 1', 'table th');
        $I->dontSee('Task 2', 'table th');
        $I->see('Task 3', 'table th');
        $I->dontSee('Task 4', 'table th');
    }
}
