<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\Task;
use App\Entity\TaskList;
use App\Form\TaskType;
use App\Tests\FunctionalTester;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class TaskCest
{
    public function it_create_a_task(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and access the page to create a task
        $id = $I->haveInRepository(TaskList::class, ['title' => 'TaskList']);
        $I->amOnPage('/task/create/'.$id);
        $I->dontSee('new task', 'table tr');

        // When I'm submitting the form
        $I->submitSymfonyForm(TaskType::BLOCK_PREFIX, ['[title]' => 'new task']);

        // Then I see the task in the Tasks list page
        $I->seeCurrentRouteIs('front_taskList_show', ['id' => $id]);
        $I->see('new task', 'table tr');
    }

    public function it_edit_a_task(FunctionalTester $I): void
    {
        // Given I have a taskList in repository, and a task
        $I->haveInRepository($taskList = new TaskList(), ['title' => 'Test']);
        $I->haveInRepository($task = new Task(), ['title' => 'Task test', 'list' => $taskList]);
        $I->amOnPage('/task/edit/'.$task->getId());

        // When I'm submitting the form
        $I->submitSymfonyForm(TaskType::BLOCK_PREFIX, [
            '[title]' => 'new task name',
            '[state]' => true,
        ]);

        // Then I see the task in the Tasks list page
        $I->seeCurrentRouteIs('front_taskList_show', ['id' => $taskList->getId()]);
        $I->see('new task name', 'table th.alert-secondary');
    }
}
