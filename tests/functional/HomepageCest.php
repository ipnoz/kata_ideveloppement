<?php

declare(strict_types=1);

namespace App\Tests\Functional;

use App\Entity\TaskList;
use App\Tests\FunctionalTester;
use App\Form\TaskListType;

/**
 * @author David Dadon <david.dadon@ipnoz.net>
 */
class HomepageCest
{
    public function it_get_the_homepage(FunctionalTester $I): void
    {
        // Given I have 3 TaskList in the repository
        $I->haveInRepository(TaskList::class, ['title' => 'TaskList 1']);
        $I->haveInRepository(TaskList::class, ['title' => 'TaskList 2']);
        $I->haveInRepository(TaskList::class, ['title' => 'TaskList 3']);

        // When
        $I->amOnPage('/');

        // Then
        $I->seeInTitle('Listing of tasks - ideveloppement');
        $I->seeElement('form[name='.TaskListType::BLOCK_PREFIX.']');
        $I->see('Create a list', 'form[name='.TaskListType::BLOCK_PREFIX.'] button[type=submit]');
        $I->see('TaskLists', 'table caption');
        $I->see('TaskList 1', 'table th');
        $I->see('TaskList 2', 'table th');
        $I->see('TaskList 3', 'table th');
    }

    public function it_click_on_a_task_list_in_the_homepage(FunctionalTester $I): void
    {
        // Given I have two TaskList in the repository
        $I->haveInRepository(TaskList::class, ['title' => 'TaskList 1']);
        $I->amOnPage('/');

        // When
        $I->click('Show', 'table tr a.btn');

        // Then
        $I->seeCurrentRouteIs('front_taskList_show');
        $I->see('TaskList 1', 'table caption');
    }

    public function it_submit_the_add_new_list_form(FunctionalTester $I): void
    {
        // Given
        $I->amOnPage('/');

        // When
        $I->submitSymfonyForm(TaskListType::BLOCK_PREFIX, ['[title]' => 'new TaskList']);

        // Then
        $I->seeCurrentRouteIs('front_taskList_show');
        $I->see('new TaskList', 'table caption');
    }

    public function it_submit_the_add_new_list_form_with_a_duplicated_task_list_title(FunctionalTester $I): void
    {
        // Given I have a taskList in repository
        $I->haveInRepository(TaskList::class, ['title' => 'new TaskList']);
        $I->amOnPage('/');

        // When I submit the form with the duplicate title
        $I->submitSymfonyForm(TaskListType::BLOCK_PREFIX, ['[title]' => 'new TaskList']);

        // Then I see I'm still in the homepage, with a form error
        $I->seeCurrentRouteIs('front_homepage');
        $I->seeFormErrorMessage('title', 'This taskList title is already in use');
    }

    public function it_delete_a_task_list(FunctionalTester $I): void
    {
        // Given I have a taskList in repository
        $I->haveInRepository(TaskList::class, ['title' => 'TaskList 1']);
        $I->amOnPage('/');
        $I->see('TaskList 1', 'table');

        // When
        $I->click('Delete', 'table tr [type=submit]');

        // Then I don't see the TaskList in the table anymore
        $I->dontSee('TaskList 1', 'table');
    }
}
